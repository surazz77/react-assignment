import React from "react";
import "./App.css";
import Path from "./Route/route";

function App() {
  return (
    <div className="App">
      <Path />
    </div>
  );
}

export default App;
