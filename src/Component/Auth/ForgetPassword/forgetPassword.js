import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import axiosInstance from "../../../Utils/http";
import { useAlert } from "react-alert";
import NavBar from "../../Ui/Navigation/navBar";
// css
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    margin: "auto 25%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(3, 2),
    display: "flex",
    flexWrap: "wrap",
    margin: "3% 25%"
  },
  header: {
    textAlign: "center"
  }
}));

function ForgetPassword(props) {
  const classes = useStyles();

  //  react alert
  const alert = useAlert();

  // initializing and handling email
  const [stateEmail, setStateEmail] = useState({
    email: ""
  });

  const onChangeEmail = e => {
    setStateEmail({
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    var data = {
      email: stateEmail.email
    };
    // api call
    axiosInstance
      .post("auth/password/reset/", data)
      .then(response => {
        console.log(response.data);
        alert.success("Password reset e-mail has been sent.");
      })
      .catch(e => {
        console.log(e);
        alert.error(e.new_password2);
      });
  };
  return (
    <React.Fragment>
      <div>
        <NavBar link="/login" authType="Login" />
        <Paper className={classes.root}>
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <Typography variant="h5" component="h3">
              <div className={classes.header}>Forget Password</div>
            </Typography>
            <TextField
              id="outlined-email-input"
              label="Email"
              className={classes.textField}
              type="email"
              name="email"
              fullWidth
              autoComplete="email"
              margin="normal"
              variant="outlined"
              onChange={onChangeEmail}
            />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.button}
            >
              submit
            </Button>
          </form>
        </Paper>
      </div>
    </React.Fragment>
  );
}
export default ForgetPassword;
