import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { useAlert } from "react-alert";
import { Link } from "react-router-dom";
import axiosInstance from "../../../Utils/http";
import NavBar from "../../Ui/Navigation/navBar";

import FacebookLogin from "react-facebook-login";

// Css
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    margin: "auto 25%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(3, 2),
    display: "flex",
    flexWrap: "wrap",
    margin: "3% 25%"
  },
  header: {
    textAlign: "center"
  }
}));

function Login(props) {
  const classes = useStyles();

  //   react alert
  const alert = useAlert();

  // initializing and handling UserName
  const [stateUserName, setStateUserName] = useState({
    userName: ""
  });

  const onChangeUserName = e => {
    setStateUserName({
      [e.target.name]: e.target.value
    });
  };

  // initializing and handling email
  const [stateEmail, setStateEmail] = useState({
    email: ""
  });

  const onChangeEmail = e => {
    setStateEmail({
      [e.target.name]: e.target.value
    });
  };

  // initializing and handling password
  const [statePassword, setStatePassword] = useState({
    email: ""
  });

  const onChangePassword = e => {
    setStatePassword({
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    var data = {
      username: stateUserName.userName,
      email: stateEmail.email,
      password: statePassword.password
    };

    //api call
    axiosInstance
      .post("auth/login/", data)
      .then(response => {
        alert.show("user is successfuly logged");
        localStorage.setItem("key", response.data["key"]);
        props.history.push("/home");
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
        alert.error("login failed");
      });
  };

  const [stateAccessToken, setStateAccessToken] = useState({
    access_token: ""
  });

  const responseFacebook = response => {
    setStateAccessToken({ access_token: response.accessToken });
    const data = {
      provider: "facebook",
      access_token: response.accessToken
    };
    axiosInstance
      .post("/oauth/login/", data)
      .then(response => {
        props.history.push("/home");
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
    console.log(response);
  };

  const componentClicked = () => {
    const data = {
      provider: "facebook",
      access_token: stateAccessToken.access_token
    };
    axiosInstance
      .post("/oauth/login/", data)
      .then(response => {
        props.history.push("/home");
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <React.Fragment>
      <div>
        <NavBar link="/register" authType="Register" />
        <Paper className={classes.root}>
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <Typography variant="h5" component="h3">
              <div className={classes.header}>User Login</div>
            </Typography>
            <TextField
              id="outlined-name"
              label="User Name"
              className={classes.textField}
              margin="normal"
              name="userName"
              fullWidth
              variant="outlined"
              onChange={onChangeUserName}
            />
            <TextField
              id="outlined-email-input"
              label="Email"
              className={classes.textField}
              type="email"
              name="email"
              fullWidth
              autoComplete="email"
              margin="normal"
              variant="outlined"
              onChange={onChangeEmail}
            />
            <TextField
              id="outlined-password-input"
              label="Password"
              className={classes.textField}
              type="password"
              name="password"
              autoComplete="current-password"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={onChangePassword}
            />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.button}
            >
              submit
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.button}
            >
              <Link
                to="/forget-password"
                style={{ textDecoration: "none", color: "white" }}
              >
                Forget Password
              </Link>
            </Button>
            <br />

            {/* login with facebook */}
            <FacebookLogin
              appId="2416961571856261"
              autoLoad={false}
              fields="name,email,picture"
              onClick={componentClicked}
              icon="fa-facebook"
              callback={responseFacebook}
            />
          </form>
        </Paper>
      </div>
    </React.Fragment>
  );
}
export default Login;
