import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import axiosInstance from "../../../Utils/http";
import { useAlert } from "react-alert";
import NavBar from "../../Ui/Navigation/navBar";

// css
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    margin: "auto 25%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(3, 2),
    display: "flex",
    flexWrap: "wrap",
    margin: "3% 25%"
  },
  header: {
    textAlign: "center"
  }
}));

function Register(props) {
  const classes = useStyles();

  // react alert
  const alert = useAlert();

  // initializing and handling UserName
  const [stateUserName, setStateUserName] = useState({
    userName: ""
  });

  const onChangeUserName = e => {
    setStateUserName({
      [e.target.name]: e.target.value
    });
  };

  // initializing and handling FirstName
  const [stateFirstName, setStateFirstName] = useState({
    firstName: ""
  });

  const onChangeFirstName = e => {
    setStateFirstName({
      [e.target.name]: e.target.value
    });
  };

  // initializing and handling lastname
  const [stateLastName, setStateLastName] = useState({
    lastName: ""
  });

  const onChangeLastName = e => {
    setStateLastName({
      [e.target.name]: e.target.value
    });
  };

  // initializing and handling email
  const [stateEmail, setStateEmail] = useState({
    email: ""
  });

  const onChangeEmail = e => {
    setStateEmail({
      [e.target.name]: e.target.value
    });
  };

  // initializing and handling password
  const [statePassword, setStatePassword] = useState({
    email: ""
  });

  const onChangePassword = e => {
    setStatePassword({
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    var data = {
      username: stateUserName.userName,
      first_name: stateFirstName.firstName,
      last_name: stateLastName.lastName,
      email: stateEmail.email,
      password: statePassword.password
    };
    // api call
    axiosInstance
      .post("users/", data)
      .then(response => {
        alert.show("Email is send for user verifications");
        props.history.push("/login");
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
        alert.error("Err in user registration");
      });
  };

  return (
    <React.Fragment>
      <div>
        <NavBar link="/login" authType="Login" />
        <Paper className={classes.root}>
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <Typography variant="h5" component="h3">
              <div className={classes.header}>User SignUp</div>
            </Typography>
            <TextField
              id="outlined-name"
              label="User Name"
              className={classes.textField}
              margin="normal"
              name="userName"
              fullWidth
              variant="outlined"
              onChange={onChangeUserName}
            />
            <TextField
              id="outlined-name"
              label="First Name"
              className={classes.textField}
              margin="normal"
              name="firstName"
              fullWidth
              variant="outlined"
              onChange={onChangeFirstName}
            />
            <TextField
              id="outlined-name"
              label="Last Name"
              className={classes.textField}
              margin="normal"
              name="lastName"
              fullWidth
              variant="outlined"
              onChange={onChangeLastName}
            />
            <TextField
              id="outlined-email-input"
              label="Email"
              className={classes.textField}
              type="email"
              name="email"
              fullWidth
              autoComplete="email"
              margin="normal"
              variant="outlined"
              onChange={onChangeEmail}
            />
            <TextField
              id="outlined-password-input"
              label="Password"
              className={classes.textField}
              type="password"
              name="password"
              autoComplete="current-password"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={onChangePassword}
            />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.button}
            >
              submit
            </Button>
          </form>
        </Paper>
      </div>
    </React.Fragment>
  );
}
export default Register;
