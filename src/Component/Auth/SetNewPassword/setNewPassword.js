import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import axiosInstance from "../../../Utils/http";
import { useAlert } from "react-alert";
import NavBar from '../../Ui/Navigation/navBar'

// css
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    margin: "auto 25%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(3, 2),
    display: "flex",
    flexWrap: "wrap",
    margin: "3% 25%"
  },
  header: {
    textAlign: "center"
  }
}));

function SetNewPassword(props) {
  const classes = useStyles();

  //  react alert
  const alert = useAlert();

  // initializing and handling password
  const [statePassword1, setStatePassword1] = useState({
    password1: ""
  });

  const onChangePassword1 = e => {
    setStatePassword1({
      [e.target.name]: e.target.value
    });
  };

  const [statePassword2, setStatePassword2] = useState({
    password2: ""
  });

  const onChangePassword2 = e => {
    setStatePassword2({
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    var data = {
      new_password1: statePassword1.password1,
      new_password2: statePassword2.password2,
      uid: props.match.params.uid,
      token: props.match.params.token
    };

    // api call
    axiosInstance
      .post("auth/password/reset/confirm/", data)
      .then(response => {
        console.log(response.data);
        alert.success("Password has been reset with the new password.");
      })
      .catch(error => {
        debugger
        console.log(e);
        alert.error(e.new_password2);
      });
  };
  return (
    <React.Fragment>
      <div>
        <NavBar link="/login" authType="Login" />
        <Paper className={classes.root}>
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <Typography variant="h5" component="h3">
              <div className={classes.header}>Forget Password Confirm</div>
            </Typography>
            <TextField
              id="outlined-password-input"
              label="New Password"
              className={classes.textField}
              type="password"
              name="password1"
              autoComplete="current-password"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={onChangePassword1}
            />
            <TextField
              id="outlined-password-input"
              label="Enter New Password Again"
              className={classes.textField}
              type="password"
              name="password2"
              autoComplete="current-password"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={onChangePassword2}
            />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.button}
            >
              submit
            </Button>
          </form>
        </Paper>
      </div>
    </React.Fragment>
  );
}
export default SetNewPassword;
