import React from "react";

import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import { Link } from "react-router-dom";

// css
const styles = theme => ({
  root: {
    width: "100%",
    marginBottom: "7%"
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    },
    textDecoration: "none",
    color: "white"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 200
    }
  },
  navElement: {
    float: ""
  }
});

const NavBar = props => {
  const { classes } = props;
  return (
    <React.Fragment>
      <div className={classes.root}>
        <AppBar position="fixed">
          <Toolbar>
            <Typography
              className={classes.title}
              variant="h6"
              color="inherit"
              noWrap
            >
              User
            </Typography>

            <div className={classes.grow} />
            <div edge="end" className={classes.navElement}>
              <Link
                to={props.link}
                style={{ textDecoration: "none", color: "white" }}
              >
                {props.authType}{" "}
              </Link>
            </div>
          </Toolbar>
        </AppBar>
        <div />
      </div>
    </React.Fragment>
  );
};

NavBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NavBar);
