import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from "../Component/Auth/Login/login";
import Register from "../Component/Auth/Register/register";
import Home from "../Component/Home/home";
import SetNewPassword from "../Component/Auth/SetNewPassword/setNewPassword";
import ForgetPassword from "../Component/Auth/ForgetPassword/forgetPassword";
import LogOut from "../Component/Auth/LogOut/Logout";

function Path() {
  return (
    <React.Fragment>
      <Router>
        <div>
          <div />
          <Route
            path="/forget-password-confirm/:uid/:token"
            component={SetNewPassword}
          />
          <Route path="/forget-password" component={ForgetPassword} />
          <Route path="/home" component={Home} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={LogOut} />
        </div>
      </Router>
    </React.Fragment>
  );
}

export default Path;
